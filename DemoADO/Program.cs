﻿using DemoADO.DAL.Entities;
using DemoADO.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO
{
    class Program
    {
        static string ServerName = @"LOGAN\SLQSERVEUR";
        static string DbName = "DemoADO";
        //static string Username = "";
        //static string Password = "";
        
        static void Main(string[] args)
        {
            ////string connectionString = $@"Server Name={ServerName};Initial Catalog={DbName};User Id={Username};Pwd={Password}";
            string connectionString =
                //pour une authentification Windows
                $@"Data Source={ServerName};Initial Catalog={DbName};Integrated Security=true";

            ////crée les outils pour construire une connection SQL
            //DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            ////crée ma connection
            //IDbConnection connection = factory.CreateConnection();

            ////Set ma connection à ma connection
            //connection.ConnectionString = connectionString;

            ////Démarrer la connection vers le server SQL
            //connection.Open();


            //IDbCommand cmd = factory.CreateCommand();
            //cmd.Connection = connection;

            ////IDbConnection cmd = connection.CreateCommand();

            //cmd.CommandText = "SELECT * FROM Student";
            //IDataReader reader = cmd.ExecuteReader();

            //while (reader.Read())
            //{
            //    //Traitement sur chacune des lignes récupérée
            //    Console.WriteLine(reader["LastName"]);
            //}


            ////Fermer la connection au Serveur SQL
            //connection.Close();


            //string lettre;
            //Console.WriteLine("Choississez une lettre de l'alphabet : ");
            //lettre = Console.ReadLine();

            //connection.Open();

            //IDbCommand cmd2 = connection.CreateCommand();
            //cmd2.CommandText = "SELECT * FROM Section WHERE Name LIKE @param";
            //IDataParameter p = cmd2.CreateParameter();
            //p.ParameterName = "@param";
            //p.Value = lettre + "%";
            //cmd2.Parameters.Add(p);

            //IDataReader r = cmd2.ExecuteReader();
            //while (r.Read())
            //{
            //    Console.WriteLine(r["Name"]);
            //}


            //connection.Close();

            SectionRepository repo = new SectionRepository(
                connectionString,
                "system.Data.SqlClient"

            );

            Section section = repo.GetById(3);
            Console.WriteLine(section.Name);

            repo.Get();
            foreach(Section s in repo.Get())
            {
                Console.WriteLine(s.Name);
            }
            Console.ReadKey();
        }
    }
}
