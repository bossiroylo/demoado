﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

INSERT INTO Section VALUES 
('INFO'),
('PHILO'),
('MATH'),
('CUISINE');

INSERT INTO Student VALUES
('Ly','Khun','MAT0001','1982-05-06',0,DEFAULT,12,1),
('Bossiroy','Logan','MAT0002','1996-11-09',1,DEFAULT,15,4),
('Sko','Maxime','MAT0003','1997-02-15',0,DEFAULT,12,2)
