﻿using DemoADO.DAL.Entities;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO.DAL.Repositories
{
    public class SectionRepository
    {
        private string _connectionString;
        private string _providerName;
        public SectionRepository(string connectionString, string providerName)
        {
            _connectionString = connectionString;
            _providerName = providerName;
        }
        public List<Section> Get(int limit = 20,int offset = 0)
        {
            using (IDbConnection connect = GetConnection())
            {
                connect.Open();
                IDbCommand cmd = CreateCommand(
                    connect,
                    "SELECT * FROM Section"
                    );
                IDataReader reader = cmd.ExecuteReader();
                List<Section> result = new List<Section>();
                while (reader.Read())
                {
                    result.Add(new Section { 
                        Id = (int)reader["id"],
                        Name = reader["Name"] as string
                    });
                };
                return result;
            }
        }

        public Section GetById(int id)
        {
            using (IDbConnection connect = GetConnection())
            {
                connect.Open();
                IDbCommand cmd = CreateCommand(
                    connect,
                    "SELECT * FROM Section WHERE id =@id",
                    new Dictionary<string, object> { { "@id", id } }
                    );
                IDataReader reader= cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new Section
                    {
                        Id = (int)reader["id"],
                        Name = reader["Name"] as string
                    };
                };
                return null;
            }
        }

        public int insert(Section s)
        {
            return 0;
            //Get the Connection
            using (IDbConnection connect = GetConnection())
            {
                //open the Connection
                connect.Open();
                //Create Command

                //Add Parameters 

                //Executer le reader

            }
        }

        public int Update(Section s)
        {
            return 0;
            //Get the Connection
            using (IDbConnection connect = GetConnection())
            {
                //open the Connection
                connect.Open();
                //Create Command

                //Add Parameters 

                //Executer le reader

            }
        }
        public bool Delete(int id)
        {
            return true;
            //Get the Connection
            using (IDbConnection connect = GetConnection())
            {
                //open the Connection
                connect.Open();
                //Create Command

                //Add Parameters 

                //Executer le reader

            }
        }

        private IDbConnection GetConnection()
        {
            DbProviderFactory factory = DbProviderFactories.GetFactory(_providerName);

            //crée ma connection
            IDbConnection connection = factory.CreateConnection();

            //Set ma connection à ma connection
            connection.ConnectionString = _connectionString;
            return connection;
        }

        private IDbCommand CreateCommand( IDbConnection connect, string text,Dictionary<string, object> parameters = null)
        {
            IDbCommand cmd = connect.CreateCommand();
            cmd.CommandText = text;
            if(parameters != null)
            {
                foreach(KeyValuePair<string, object> kvp in parameters)
                {
                    IDbDataParameter p = cmd.CreateParameter();
                    p.ParameterName = kvp.Key;
                    p.Value = kvp.Value;
                    cmd.Parameters.Add(p);
                }
            }
            return cmd;
        }
    }
}
