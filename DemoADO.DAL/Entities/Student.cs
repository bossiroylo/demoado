﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO.DAL.Entities
{
    public class Student
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Matricule { get; set; }
        public DateTime? BirthDate { get; set; }
        public bool AnanasFan { get; set; }
        public int yearResult { get; set; }
        public DateTime? InscriptionDate { get; set; }
        public int? SectionId { get; set; }
    }
}
